package quickstart;

import java.awt.Container;
import java.util.*;
import com.vmware.vim25.*;

import javax.xml.ws.BindingProvider;

public class Quickstart {
	
public static void main(String[] args) throws InvalidPropertyFaultMsg {
	
    String username   = "administrator@vsphere.local";
    String password   = "restrict";
	
	VimService vimService = new VimService();
	VimPortType vimPort = vimService.getVimPort();

	Map<String, Object> ctxt = ((BindingProvider) vimPort).getRequestContext();
	ctxt.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, "https://voyager.sourceware.org.br/sdk/vimService");
	ctxt.put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
	
	ManagedObjectReference serviceInstance = new ManagedObjectReference();
	serviceInstance.setType("ServiceInstance");
	serviceInstance.setValue("ServiceInstance");

	try {
		ServiceContent serviceContent =
				vimPort.retrieveServiceContent(serviceInstance);
		vimPort.login(serviceContent.getSessionManager(), username, password, null);

		collectProperties(vimPort, serviceContent);

		vimPort.logout(serviceContent.getSessionManager());

	} catch (RuntimeFaultFaultMsg e) {
		e.printStackTrace();
	} catch (InvalidLocaleFaultMsg e) {
		e.printStackTrace();
	} catch (InvalidLoginFaultMsg e) {
		e.printStackTrace();
	}
}

private static void collectProperties(VimPortType vimPort, ServiceContent serviceContent) 
        throws InvalidPropertyFaultMsg, RuntimeFaultFaultMsg {

	ManagedObjectReference viewMgrRef = serviceContent.getViewManager();		
    ManagedObjectReference propertyCollector = serviceContent.getPropertyCollector();
    

    List<String> dataCenter = new ArrayList<String>();
    dataCenter.add("Datacenter");
    
    ManagedObjectReference cViewRef = vimPort.createContainerView(viewMgrRef, serviceContent.getRootFolder(), dataCenter, true);
    
    ObjectSpec objectSpec = new ObjectSpec();
    objectSpec.setObj(cViewRef);
    objectSpec.setSkip(true);
    
    TraversalSpec tSpec = new TraversalSpec();
    tSpec.setName("traverseEntities");
    tSpec.setPath("view");
    tSpec.setSkip(false);
    tSpec.setType("ContainerView");

    objectSpec.getSelectSet().add(tSpec);
    
    PropertySpec pSpec = new PropertySpec();
    pSpec.setType("Datacenter");
    pSpec.getPathSet().add("name");
    
    PropertyFilterSpec propertyFilterSpec = new PropertyFilterSpec();
    propertyFilterSpec.getObjectSet().add(objectSpec);
    propertyFilterSpec.getPropSet().add(pSpec);
    
    List<PropertyFilterSpec> propertyFilterSpecList = new ArrayList<PropertyFilterSpec>();
    propertyFilterSpecList.add(propertyFilterSpec);

    RetrieveOptions retrieveOptions = new RetrieveOptions();

    RetrieveResult result = vimPort.retrievePropertiesEx(propertyCollector, propertyFilterSpecList, retrieveOptions);

    if (result != null) {
    	for (ObjectContent objectContent : result.getObjects()) {
    		List<DynamicProperty> properties = objectContent.getPropSet();
    		for (DynamicProperty property : properties) {
    			if (property.getName().equals("name")) {
    				collectPropertiesComputeResource(vimPort, serviceContent, (String) property.getVal());
    			}
    		}
    	}
    } else {
      System.out.println("O conteúdo do objeto está vazio");
    }
    
}

private static void collectPropertiesComputeResource(VimPortType vimPort, ServiceContent serviceContent, String dataCenterName) 
        throws InvalidPropertyFaultMsg, RuntimeFaultFaultMsg {
	
    ManagedObjectReference propertyCollector = serviceContent.getPropertyCollector();        
    ManagedObjectReference hostFolder = vimPort.findByInventoryPath(serviceContent.getSearchIndex(), dataCenterName + "/host");
    
    ObjectSpec objectSpec = new ObjectSpec();
    objectSpec.setObj(hostFolder);
    objectSpec.setSkip(true);
    
    TraversalSpec tSpec = new TraversalSpec();
    tSpec.setName("traverseFolder");
    tSpec.setPath("childEntity");
    tSpec.setSkip(false);
    tSpec.setType("Folder");

    objectSpec.getSelectSet().add(tSpec);
    
    PropertySpec pSpec = new PropertySpec();
    pSpec.setType("ComputeResource");
    pSpec.getPathSet().add("host");
    
    PropertyFilterSpec propertyFilterSpec = new PropertyFilterSpec();
    propertyFilterSpec.getObjectSet().add(objectSpec);
    propertyFilterSpec.getPropSet().add(pSpec);
    
    List<PropertyFilterSpec> propertyFilterSpecList = new ArrayList<PropertyFilterSpec>();
    propertyFilterSpecList.add(propertyFilterSpec);

    RetrieveOptions retrieveOptions = new RetrieveOptions();

    RetrieveResult result = vimPort.retrievePropertiesEx(propertyCollector, propertyFilterSpecList, retrieveOptions);

    if (result != null) {
    	for (ObjectContent objectContent : result.getObjects()) {
    		List<DynamicProperty> properties = objectContent.getPropSet();
    		for (DynamicProperty property : properties) {
    			collectPropertiesHostSystem(vimPort, serviceContent, (ArrayOfManagedObjectReference) property.getVal());
    		}
    	}
    } else {
      System.out.println("O conteúdo do objeto está vazio");
    }
    
}

private static void collectPropertiesHostSystem(VimPortType vimPort, ServiceContent serviceContent, ArrayOfManagedObjectReference o) 
        throws InvalidPropertyFaultMsg, RuntimeFaultFaultMsg {
	
	ManagedObjectReference viewMgrRef = serviceContent.getViewManager();		
    ManagedObjectReference propertyCollector = serviceContent.getPropertyCollector();
    

    List<String> dataCenter = new ArrayList<String>();
    dataCenter.add("HostSystem");
    
    ManagedObjectReference cViewRef = vimPort.createContainerView(viewMgrRef, serviceContent.getRootFolder(), dataCenter, true);
    
    ObjectSpec objectSpec = new ObjectSpec();
    objectSpec.setObj(cViewRef);
    objectSpec.setSkip(true);
    
    TraversalSpec tSpec = new TraversalSpec();
    tSpec.setName("traverseEntities");
    tSpec.setPath("view");
    tSpec.setSkip(false);
    tSpec.setType("ContainerView");

    objectSpec.getSelectSet().add(tSpec);
    
    PropertySpec pSpec = new PropertySpec();
    pSpec.setType("HostSystem");
    pSpec.getPathSet().add("name");
    pSpec.getPathSet().add("datastore");
    
    PropertyFilterSpec propertyFilterSpec = new PropertyFilterSpec();
    propertyFilterSpec.getObjectSet().add(objectSpec);
    propertyFilterSpec.getPropSet().add(pSpec);
    
    List<PropertyFilterSpec> propertyFilterSpecList = new ArrayList<PropertyFilterSpec>();
    propertyFilterSpecList.add(propertyFilterSpec);

    RetrieveOptions retrieveOptions = new RetrieveOptions();

    RetrieveResult result = vimPort.retrievePropertiesEx(propertyCollector, propertyFilterSpecList, retrieveOptions);

    if (result != null) {
    	for (ObjectContent objectContent : result.getObjects()) {
    		List<DynamicProperty> properties = objectContent.getPropSet();
    		for (DynamicProperty property : properties) {
    			if (property.getName().equals("datastore")) {
    				collectPropertiesDatastore(vimPort, serviceContent, (ArrayOfManagedObjectReference) property.getVal());
    			}

    		}
    	}
    } else {
      System.out.println("O conteúdo do objeto está vazio");
    }
    
}

private static void collectPropertiesDatastore(VimPortType vimPort, ServiceContent serviceContent, ArrayOfManagedObjectReference o) 
        throws InvalidPropertyFaultMsg, RuntimeFaultFaultMsg {
	
	ManagedObjectReference viewMgrRef = serviceContent.getViewManager();		
    ManagedObjectReference propertyCollector = serviceContent.getPropertyCollector();
    

    List<String> dataCenter = new ArrayList<String>();
    dataCenter.add("Datastore");
    
    ManagedObjectReference cViewRef = vimPort.createContainerView(viewMgrRef, serviceContent.getRootFolder(), dataCenter, true);
    
    ObjectSpec objectSpec = new ObjectSpec();
    objectSpec.setObj(cViewRef);
    objectSpec.setSkip(true);
    
    TraversalSpec tSpec = new TraversalSpec();
    tSpec.setName("traverseEntities");
    tSpec.setPath("view");
    tSpec.setSkip(false);
    tSpec.setType("ContainerView");

    objectSpec.getSelectSet().add(tSpec);
    
    PropertySpec pSpec = new PropertySpec();
    pSpec.setType("Datastore");
    pSpec.getPathSet().add("summary.name");
    pSpec.getPathSet().add("summary.type");
    pSpec.getPathSet().add("summary.url");
    pSpec.getPathSet().add("summary.capacity");
    pSpec.getPathSet().add("summary.freeSpace");
    // pSpec.setAll(true);

    
    PropertyFilterSpec propertyFilterSpec = new PropertyFilterSpec();
    propertyFilterSpec.getObjectSet().add(objectSpec);
    propertyFilterSpec.getPropSet().add(pSpec);
    
    List<PropertyFilterSpec> propertyFilterSpecList = new ArrayList<PropertyFilterSpec>();
    propertyFilterSpecList.add(propertyFilterSpec);

    RetrieveOptions retrieveOptions = new RetrieveOptions();

    RetrieveResult result = vimPort.retrievePropertiesEx(propertyCollector, propertyFilterSpecList, retrieveOptions);

    if (result != null) {
    	for (ObjectContent objectContent : result.getObjects()) {
    		List<DynamicProperty> properties = objectContent.getPropSet();
    		for (DynamicProperty property : properties) {
    			System.out.println(property.getVal());    			
    		}
    	}
    } else {
      System.out.println("O conteúdo do objeto está vazio");
    }
    
}


}
